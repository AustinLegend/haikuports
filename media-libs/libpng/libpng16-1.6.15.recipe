SUMMARY="Portable Network Graphics library"
DESCRIPTION="
libpng is the official PNG reference library. It supports almost all PNG \
features, is extensible, and has been extensively tested for over 17 years
"
HOMEPAGE="http://www.libpng.org"
COPYRIGHT="
	1998-2014 Glenn Randers-Pehrson
	1996-1997 Andreas Dilger
	1995-1996 Guy Eric Schalnat, Group 42, Inc.
	"
LICENSE="LibPNG"
SRC_URI="http://prdownloads.sourceforge.net/libpng/libpng-$portVersion.tar.gz"
CHECKSUM_SHA256="dc4f90ff3ba4559b5eb653da9058e3d862d5e8162fe2612f9339a6f6d2a2139d"
SOURCE_DIR="libpng-$portVersion"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64 arm"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	libpng16$secondaryArchSuffix = $portVersion compat >= 1.6
	lib:libpng$secondaryArchSuffix = 16.15.0 compat >= 16
	lib:libpng16$secondaryArchSuffix = 16.15.0 compat >= 16
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libz$secondaryArchSuffix >= 1.0.4
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtool >= 2.4.2
	cmd:make
	"

BUILD()
{
	autoreconf -f -i
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libpng libpng16
	fixPkgconfig

	# devel package
	packageEntries devel \
		$binDir \
		$developDir \
		$manDir/man3
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libpng16${secondaryArchSuffix}_devel = $portVersion compat >= 1.6
	cmd:libpng_config$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:libpng16_config$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:pngfix$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:png_fix_itxt$secondaryArchSuffix = $portVersion compat >= 1.6
	devel:libpng$secondaryArchSuffix = 16.15.0 compat >= 16
	devel:libpng16$secondaryArchSuffix = 16.15.0 compat >= 16
	"
REQUIRES_devel="
	libpng16$secondaryArchSuffix == $portVersion base
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libz$secondaryArchSuffix
	"
CONFLICTS_devel="
	libpng${secondaryArchSuffix}_devel
	"
