From 9e62b8084f970f94b9df86f0764284b042bf822b Mon Sep 17 00:00:00 2001
From: Kacper Kasper <kacperkasper@gmail.com>
Date: Mon, 7 Apr 2014 16:48:59 +0200
Subject: Haiku support


diff --git a/Makefile b/Makefile
index e6f08a7..24b9d22 100644
--- a/Makefile
+++ b/Makefile
@@ -179,12 +179,15 @@ glewmx.pc: glew.pc.in
 
 # GLEW utility programs
 
-BIN.LIBS = -Llib $(LDFLAGS.DYNAMIC) -l$(NAME) $(LDFLAGS.EXTRA) $(LDFLAGS.GL)
+BIN.LIBS = -Llib $(LDFLAGS.DYNAMIC) -l$(NAME) $(LDFLAGS.EXTRA) $(LDFLAGS.GL) -lbe -lstdc++
 
 GLEWINFO.BIN       := glewinfo$(BIN.SUFFIX)
 GLEWINFO.BIN.SRC   := src/glewinfo.c
+GLEWINFO.BIN.HAIKU := src/glewinfo_haiku.cpp
 GLEWINFO.BIN.OBJ   := $(addprefix tmp/$(SYSTEM)/default/shared/,$(notdir $(GLEWINFO.BIN.SRC)))
 GLEWINFO.BIN.OBJ   := $(GLEWINFO.BIN.OBJ:.c=.o)
+GLEWINFO.BIN.HAIKU_OBJ   := $(addprefix tmp/$(SYSTEM)/default/shared/,$(notdir $(GLEWINFO.BIN.HAIKU)))
+GLEWINFO.BIN.HAIKU_OBJ   := $(GLEWINFO.BIN.HAIKU_OBJ:.cpp=.o)
 
 VISUALINFO.BIN     := visualinfo$(BIN.SUFFIX)
 VISUALINFO.BIN.SRC := src/visualinfo.c
@@ -202,8 +205,8 @@ endif
 bin:
 	mkdir bin
 
-bin/$(GLEWINFO.BIN): $(GLEWINFO.BIN.OBJ) lib/$(LIB.SHARED)
-	$(CC) $(CFLAGS) -o $@ $(GLEWINFO.BIN.OBJ) $(BIN.LIBS)
+bin/$(GLEWINFO.BIN): $(GLEWINFO.BIN.OBJ) $(GLEWINFO.BIN.HAIKU_OBJ) lib/$(LIB.SHARED)
+	$(CC) $(CFLAGS) -o $@ $(GLEWINFO.BIN.OBJ) $(GLEWINFO.BIN.HAIKU_OBJ) $(BIN.LIBS)
 ifneq ($(STRIP),)
 	$(STRIP) -x $@
 endif
@@ -218,6 +221,10 @@ $(GLEWINFO.BIN.OBJ): $(GLEWINFO.BIN.SRC) include/GL/glew.h include/GL/wglew.h in
 	@mkdir -p $(dir $@)
 	$(CC) -DGLEW_NO_GLU $(CFLAGS) $(CFLAGS.SO) -o $@ -c $<
 
+$(GLEWINFO.BIN.HAIKU_OBJ): $(GLEWINFO.BIN.HAIKU) include/GL/glew.h include/GL/wglew.h include/GL/glxew.h
+	@mkdir -p $(dir $@)
+	$(CXX) -DGLEW_NO_GLU $(CXXFLAGS) $(CXXFLAGS.SO) -o $@ -c $<
+
 $(VISUALINFO.BIN.OBJ): $(VISUALINFO.BIN.SRC) include/GL/glew.h include/GL/wglew.h include/GL/glxew.h
 	@mkdir -p $(dir $@)
 	$(CC) -DGLEW_NO_GLU $(CFLAGS) $(CFLAGS.SO) -o $@ -c $<
diff --git a/config/Makefile.haiku b/config/Makefile.haiku
new file mode 100644
index 0000000..57bf3d6
--- /dev/null
+++ b/config/Makefile.haiku
@@ -0,0 +1,30 @@
+NAME = $(GLEW_NAME)
+CC = cc
+CXX = g++
+LD = cc
+ifneq (undefined, $(origin GLEW_MX))
+	CFLAGS.EXTRA = -DGLEW_MX
+endif
+
+LDFLAGS.GL = -lGL
+LDFLAGS.STATIC = -Wl,-Bstatic
+LDFLAGS.DYNAMIC = -Wl,-Bdynamic
+
+NAME = GLEW
+WARN = -Wall -W
+POPT = -O2
+
+BIN.SUFFIX =
+
+LIB.SONAME = lib$(NAME).so.$(SO_MAJOR)
+LIB.DEVLNK = lib$(NAME).so
+LIB.SHARED = lib$(NAME).so.$(SO_VERSION)
+LIB.STATIC = lib$(NAME).a
+LDFLAGS.SO = -shared -Wl,-soname=$(LIB.SONAME)
+
+LIB.SONAME.MX = lib$(NAME)mx.so.$(SO_MAJOR)
+LIB.DEVLNK.MX = lib$(NAME)mx.so
+LIB.SHARED.MX = lib$(NAME)mx.so.$(SO_VERSION)
+LIB.STATIC.MX = lib$(NAME)mx.a
+LDFLAGS.SO.MX = -shared -Wl,-soname=$(LIB.SONAME.MX)
+
diff --git a/src/glew.c b/src/glew.c
index d075b52..daff07d 100644
--- a/src/glew.c
+++ b/src/glew.c
@@ -33,7 +33,7 @@
 
 #if defined(_WIN32)
 #  include <GL/wglew.h>
-#elif !defined(__ANDROID__) && !defined(__native_client__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
+#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
 #  include <GL/glxew.h>
 #endif
 
@@ -66,7 +66,7 @@
 #  define GLXEW_CONTEXT_ARG_DEF_LIST void
 #endif /* GLEW_MX */
 
-#if defined(__sgi) || defined (__sun) || defined(GLEW_APPLE_GLX)
+#if defined(__sgi) || defined (__sun) || defined(GLEW_APPLE_GLX) || defined(__HAIKU__)
 #include <dlfcn.h>
 #include <stdio.h>
 #include <stdlib.h>
@@ -162,7 +162,7 @@ void* NSGLGetProcAddress (const GLubyte *name)
 #  define glewGetProcAddress(name) wglGetProcAddress((LPCSTR)name)
 #elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)
 #  define glewGetProcAddress(name) NSGLGetProcAddress(name)
-#elif defined(__sgi) || defined(__sun)
+#elif defined(__sgi) || defined(__sun) || defined(__HAIKU__)
 #  define glewGetProcAddress(name) dlGetProcAddress(name)
 #elif defined(__ANDROID__)
 #  define glewGetProcAddress(name) NULL /* TODO */
@@ -12324,7 +12324,7 @@ GLenum GLEWAPIENTRY wglewContextInit (WGLEW_CONTEXT_ARG_DEF_LIST)
   return GLEW_OK;
 }
 
-#elif !defined(__ANDROID__) && !defined(__native_client__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
+#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
 
 PFNGLXGETCURRENTDISPLAYPROC __glewXGetCurrentDisplay = NULL;
 
@@ -13432,7 +13432,7 @@ GLenum glxewContextInit (GLXEW_CONTEXT_ARG_DEF_LIST)
   return GLEW_OK;
 }
 
-#endif /* !defined(__ANDROID__) && !defined(__native_client__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX)) */
+#endif /* !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX)) */
 
 /* ------------------------------------------------------------------------ */
 
@@ -13472,7 +13472,7 @@ GLboolean glewExperimental = GL_FALSE;
 
 #if defined(_WIN32)
 extern GLenum GLEWAPIENTRY wglewContextInit (void);
-#elif !defined(__ANDROID__) && !defined(__native_client__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
+#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX))
 extern GLenum GLEWAPIENTRY glxewContextInit (void);
 #endif /* _WIN32 */
 
@@ -13483,7 +13483,7 @@ GLenum GLEWAPIENTRY glewInit (void)
   if ( r != 0 ) return r;
 #if defined(_WIN32)
   return wglewContextInit();
-#elif !defined(__ANDROID__) && !defined(__native_client__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX)) /* _UNIX */
+#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && (!defined(__APPLE__) || defined(GLEW_APPLE_GLX)) /* _UNIX */
   return glxewContextInit();
 #else
   return r;
@@ -17616,7 +17616,7 @@ GLboolean GLEWAPIENTRY wglewIsSupported (const char* name)
   return ret;
 }
 
-#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#elif !defined(__ANDROID__) && !defined(__native_client__) && !defined(__HAIKU__) && !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
 
 #if defined(GLEW_MX)
 GLboolean glxewContextIsSupported (const GLXEWContext* ctx, const char* name)
diff --git a/src/glewinfo.c b/src/glewinfo.c
index ad44f72..d65d702 100644
--- a/src/glewinfo.c
+++ b/src/glewinfo.c
@@ -35,7 +35,7 @@
 #include <GL/glew.h>
 #if defined(_WIN32)
 #include <GL/wglew.h>
-#elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#elif !defined(__APPLE__) && !defined(__HAIKU__) || defined(GLEW_APPLE_GLX)
 #include <GL/glxew.h>
 #endif
 
@@ -51,7 +51,7 @@ GLEWContext _glewctx;
 #ifdef _WIN32
 WGLEWContext _wglewctx;
 #define wglewGetContext() (&_wglewctx)
-#elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#elif !defined(__APPLE__)&& !defined(__HAIKU__)  || defined(GLEW_APPLE_GLX)
 GLXEWContext _glxewctx;
 #define glxewGetContext() (&_glxewctx)
 #endif
@@ -59,7 +59,7 @@ GLXEWContext _glxewctx;
 
 #if defined(_WIN32)
 GLboolean glewCreateContext (int* pixelformat);
-#elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#elif !defined(__APPLE__) && !defined(__HAIKU__)  || defined(GLEW_APPLE_GLX)
 GLboolean glewCreateContext (const char* display, int* visual);
 #else
 GLboolean glewCreateContext ();
@@ -10613,6 +10613,13 @@ void glewDestroyContext ()
 
 /* ------------------------------------------------------------------------ */
 
+#elif defined(__HAIKU__)
+
+extern GLboolean glewCreateContext ();
+extern void glewDestroyContext ();
+
+/* ------------------------------------------------------------------------ */
+
 #else /* __UNIX || (__APPLE__ && GLEW_APPLE_GLX) */
 
 Display* dpy = NULL;
diff --git a/src/glewinfo_haiku.cpp b/src/glewinfo_haiku.cpp
new file mode 100644
index 0000000..3d32d00
--- /dev/null
+++ b/src/glewinfo_haiku.cpp
@@ -0,0 +1,93 @@
+/*
+ * glewinfo Haiku glue code
+ * Copyright 2013 Kacper Kasper <kacperkasper@gmail.com>
+ * All rights reserved. Distributed under the terms of the MIT license.
+ */
+
+#include <Application.h>
+#include <Window.h>
+#include <GLView.h>
+
+class GLEWInfoView : public BGLView {
+public:
+	GLEWInfoView(BRect frame, uint32 type);
+	virtual void AttachedToWindow(void);
+
+	void ContextInit() { LockGL(); }
+	void ContextDestroy() { UnlockGL(); }
+};
+
+GLEWInfoView::GLEWInfoView(BRect frame, uint32 type)
+	: BGLView(frame, "GLEWInfoView", B_FOLLOW_ALL_SIDES, 0, type)
+{
+}
+
+void GLEWInfoView::AttachedToWindow(void)
+{
+	LockGL();
+	BGLView::AttachedToWindow();
+	UnlockGL();
+}
+
+class GLEWInfoWindow : public BWindow {
+public:
+	GLEWInfoWindow(BRect frame, uint32 type);
+	virtual bool QuitRequested();
+
+private:
+	GLEWInfoView *view;
+};
+
+GLEWInfoWindow::GLEWInfoWindow(BRect frame, uint32 type)
+	: BWindow(frame, "GLEWInfoWindow", B_TITLED_WINDOW, B_QUIT_ON_WINDOW_CLOSE)
+{
+	AddChild(view = new GLEWInfoView(Bounds(), type));
+	view->ContextInit();
+}
+
+bool GLEWInfoWindow::QuitRequested()
+{
+	view->ContextDestroy();
+
+	view->RemoveSelf();
+
+	delete view;
+
+	return true;
+}
+
+class GLEWInfoApp : public BApplication
+{
+public:
+	GLEWInfoApp();
+	~GLEWInfoApp();
+
+private:
+	GLEWInfoWindow *window;
+};
+
+GLEWInfoApp::GLEWInfoApp()
+	: BApplication("application/x-vnd.glew-GLEWInfo")
+{
+	BRect rect = BRect(0, 0, 1, 1);
+	window = new GLEWInfoWindow(rect, BGL_RGB | BGL_DOUBLE);
+}
+
+GLEWInfoApp::~GLEWInfoApp()
+{
+	window->Quit();
+}
+
+GLEWInfoApp *app;
+
+extern "C" GLboolean glewCreateContext ()
+{
+	app = new GLEWInfoApp();
+
+  	return GL_FALSE;
+}
+
+extern "C" void glewDestroyContext ()
+{
+	delete app;
+}
diff --git a/src/visualinfo.c b/src/visualinfo.c
index b8bb59e..742fdd6 100644
--- a/src/visualinfo.c
+++ b/src/visualinfo.c
@@ -37,7 +37,7 @@
 #include <GL/wglew.h>
 #elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)
 #include <AGL/agl.h>
-#else
+#elif !defined(__HAIKU__)
 #include <GL/glxew.h>
 #endif
 
@@ -47,7 +47,7 @@ GLEWContext _glewctx;
 #  ifdef _WIN32
 WGLEWContext _wglewctx;
 #    define wglewGetContext() (&_wglewctx)
-#  elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#  elif !defined(__APPLE__) && !defined(__HAIKU__) || defined(GLEW_APPLE_GLX)
 GLXEWContext _glxewctx;
 #    define glxewGetContext() (&_glxewctx)
 #  endif
@@ -61,7 +61,7 @@ typedef struct GLContextStruct
   HGLRC rc;
 #elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)
   AGLContext ctx, octx;
-#else
+#elif !defined(__HAIKU__)
   Display* dpy;
   XVisualInfo* vi;
   GLXContext ctx;
@@ -129,7 +129,7 @@ main (int argc, char** argv)
   err = glewContextInit(glewGetContext());
 #  ifdef _WIN32
   err = err || wglewContextInit(wglewGetContext());
-#  elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
+#  elif !defined(__APPLE__) && !defined(__HAIKU__) || defined(GLEW_APPLE_GLX)
   err = err || glxewContextInit(glxewGetContext());
 #  endif
 #else
@@ -182,6 +182,8 @@ main (int argc, char** argv)
   }
 #elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)
   
+#elif defined(__HAIKU__)
+
 #else
   /* GLX extensions */
   fprintf(file, "GLX extensions (GLX_): \n");
@@ -598,6 +600,16 @@ VisualInfo (GLContext* ctx)
 */
 }
 
+/* ---------------------------------------------------------------------- */
+
+#elif defined(__HAIKU__)
+
+void
+VisualInfo (GLContext* ctx)
+{
+	// TODO
+}
+
 #else /* GLX */
 
 void
@@ -1074,6 +1086,31 @@ void DestroyContext (GLContext* ctx)
 
 /* ------------------------------------------------------------------------ */
 
+#elif defined(__HAIKU__)
+
+void
+InitContext (GLContext* ctx)
+{
+	// TODO
+}
+
+
+GLboolean
+CreateContext (GLContext* ctx)
+{
+	// TODO
+	return GL_FALSE;
+}
+
+
+void
+DestroyContext (GLContext* ctx)
+{
+	// TODO
+}
+
+/* ------------------------------------------------------------------------ */
+
 #else /* __UNIX || (__APPLE__ && GLEW_APPLE_GLX) */
 
 void InitContext (GLContext* ctx)
-- 
1.8.3.4

